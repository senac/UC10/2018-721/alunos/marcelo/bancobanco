package Conteudo;

import java.util.List;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Agenda {
    
    private String nome;
    private String cor;
    private List<Pessoa> contatos;

    public Agenda() {
        this.contatos = new ArrayList<>();
    }

    public Agenda(String nome) {
        this();
        this.nome = nome;
    }

    public Agenda(String nome, String cor) {
        this(nome);
        this.cor = cor;

    }

    public void adicionarContato(Pessoa p) {

       this.contatos.add(p);

    }

    public void removerContato(Pessoa p) {
        if (this.isContatoNaAgenda(p)) {
            this.contatos.remove(p);
        } else {
            throw new RuntimeException("Contato não esta na agenda !");
        }
    }

    public void removerContato(String nome) {
        boolean achei = false;
        for (int i = 0; i < this.contatos.size(); i++) {
            if (this.contatos.get(i).getNome().equalsIgnoreCase(nome)) {
                this.contatos.remove(i);
                achei = true;
                break;
            }
        }

        if (!achei) {
            throw new RuntimeException("Contato não esta na agenda !");
        }

    }

    public int getQuantidadeContatos() {
        return this.contatos.size();
    }

    public Pessoa buscarContato(String nome) {
        for (int i = 0; i < this.getQuantidadeContatos(); i++) {
            if (this.contatos.get(i).getNome().equals(nome)) {
                return this.contatos.get(i);
            }
        }

        return null;
    }

    public List<Pessoa> getContatos() {
        return contatos;
    }

    public List<Pessoa> getContatosFiltrado(String filtro) {

        List<Pessoa> contatosFiltrado = new ArrayList<>();

        for (Pessoa p : this.contatos) {

            if (p.getNome().contains(filtro) || p.getTelefone().contains(filtro)) {
                contatosFiltrado.add(p);
            }

        }

        if (contatosFiltrado.isEmpty()) {
            throw new RuntimeException("Contato não encontrado!!!!");
        }

        return contatosFiltrado;

    }

    public String getListaContatosParaImpressao() {

        String listaContatos = "";

        for (Pessoa contato : contatos) {
            listaContatos += contato.toString() + "\n";
        }

        return listaContatos;

    }

    public boolean isContatoNaAgenda(Pessoa p) {
        return this.contatos.contains(p);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public boolean isVazia() {
        return this.contatos.isEmpty();
    }

    public void exportar() {
        
        File file = new File("Agenda.txt");
                try{
                    FileWriter fw;
                    if(!file.exists()){
                        file.createNewFile();
                        fw = new FileWriter(file);
                    }else{
                        fw = new FileWriter(file, true);
                    }
                    
           for (Pessoa x : this.contatos  ) {
               
               fw.write(x.toString() +"\n");
           }   
                    fw.close();
                    
                }catch (IOException ex){
                    ex.printStackTrace();
                }
   }
            

    public void load() throws IOException {

        File file = new File("agenda.txt");

        Scanner scanner = new Scanner(file);

        while (scanner.hasNext()) {
            String nome = scanner.useDelimiter(",").next();
            String telefone = scanner.useDelimiter("\n").next().replace(",", "");
            Pessoa p = new Pessoa(nome, telefone);
            this.adicionarContato(p);

        }

    }

    public String getTelefone() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
