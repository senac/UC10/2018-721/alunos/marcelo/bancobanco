
package Conteudo;

import java.util.List;

public interface DAO<M> {

    void inserir(M objeto);

    void atualizar(M objeto);

    void delete(int id);

    List<M> listaTodos();

    M buscarPorId(int id);

}