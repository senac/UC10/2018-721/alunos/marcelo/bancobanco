package br.com.senac.gui;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class JFrameModel extends JFrame {
    
    public JFrameModel() {
        this.setVisible(true);
    }
     public void showMessageInformacao(String mensagem) {
        JOptionPane.showMessageDialog(this, mensagem, "Aviso", JOptionPane.INFORMATION_MESSAGE);
    }

    public void showMessageErro(String mensagem) {
        JOptionPane.showMessageDialog(this, mensagem, "Erro", JOptionPane.ERROR_MESSAGE);
    }

    public void showMessageAlerta(String mensagem) {
        JOptionPane.showMessageDialog(this, mensagem, "Alerta", JOptionPane.WARNING_MESSAGE);
    }
    
}
