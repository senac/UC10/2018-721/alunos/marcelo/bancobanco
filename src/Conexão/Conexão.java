
package Conexão;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexão {
    
    private static final String URL = "jdbc:mysql://localhost:3306/AgendaBanco";
    private static final String USER = "root";
    private static final String PASSWORD = "123456";
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    
    
    public static Connection getConnection() {
    
        Connection connection = null;

        try {
            
            Class.forName(DRIVER); 
            connection = (Connection) DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("Conectado");

        } catch (ClassNotFoundException ex) {
            System.out.println("Erro ao Carregar o Driver do Banco.");
            ex.printStackTrace();

        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("Erro ao Conectar no Banco");
        }

        return connection;
    }

   
}
