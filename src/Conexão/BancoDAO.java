
package Conexão;
    
import Conexão.Banco;
import Conteudo.DAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BancoDAO implements DAO<Banco> {

    @Override
    public void inserir(Banco banco) {
        Connection connection = null;
        try {
            String query = "INSERT INTO  (NOME , TELEFONE) VALUES (?, ? , ? , ?)";
            connection = Conexão.getConnection();
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, banco.getNome());
            ps.setString(2, banco.getTelefone());
            
            int linhas = ps.executeUpdate();
            if (linhas > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                int id = rs.getInt(1);
                banco.setId(id);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void atualizar(Banco banco) {

        Connection connection = null;
        try {
            
            String query = "UPDATE AgendaBanco SET NOME = ? ,TELEFONE = ? , WHERE ID = ? ";
            connection = Conexão.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, banco.getNome());
            ps.setString(2, banco.getTelefone());
            ps.setInt(5, banco.getId());
            ps.executeUpdate();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Banco> listaTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Banco buscarPorId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Banco buscarPorNome(String nome) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}