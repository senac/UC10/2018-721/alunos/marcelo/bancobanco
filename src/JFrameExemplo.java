
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


public class JFrameExemplo {
    
    public static void main(String[] args){
    
        JFrame frame = new JFrame("Login");
        frame.setBounds(321 , 321 , 321 , 321);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new FlowLayout());
        
        JLabel jLabel = new JLabel("Nome: ");
        frame.add(jLabel);
        
        JTextField Nome = new JTextField(20);
        frame.add(Nome);
        
        JLabel jLabel2 = new JLabel("Senha: ");
        frame.add(jLabel2);
        
        JTextField Senha = new JTextField(20);
        frame.add(Senha);
        
        
        JButton jbutton = new JButton("Login");
        frame.add(jbutton);
        
        jbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                if ("admin".equals(Nome.getText())
                && "admin".equals(Senha.getText())){
                JOptionPane.showMessageDialog(jbutton, "Bem Vindo", "Logado Com Sucesso", 1);
                  
                } else {
                if ("".equals(Nome.getText())
                && "".equals(Senha.getText())){
                JOptionPane.showMessageDialog(jbutton, "Favor Preencher os Dados", "Alerta", 2);
            } else {
               JOptionPane.showMessageDialog(jbutton, "Usuário ou Senha Incorreto", "Falha na Autenticação", 0);     
                }
            }
            }
        });
        
        frame.setVisible(true);
        
    }
    
}
